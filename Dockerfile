FROM openjdk:21
WORKDIR /app
COPY . /app

RUN ./mvnw package -Dmaven.test.skip

EXPOSE 8081

CMD [ "java", "-jar", "/app/target/ApkDownloader-0.0.1-SNAPSHOT.jar"]