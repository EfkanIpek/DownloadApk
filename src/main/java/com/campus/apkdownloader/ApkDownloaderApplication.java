package com.campus.apkdownloader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApkDownloaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApkDownloaderApplication.class, args);
    }

}
